package util;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.LinkedList;

public class Cell {
	
	private static long current_id = 0; 
	
	private long id;
	private long time;
	private float centroid[] = {0.f,0.f};
	private float area;
	private Rectangle boundary;
	private Polygon outline;
	private long track_id;
	private LinkedList<Link> links;
	
	
	
	public Cell(Polygon poly, long time)
	{
		this.id = current_id;
		this.time = time;
		this.track_id = -1;
		this.outline = poly;
		this.centroid = computeCentroid(poly);
		this.area = computeArea(poly);
		this.boundary = poly.getBounds();
		this.links = new LinkedList<Link>();
		++current_id;
	}

	public static float[] computeCentroid(Polygon poly)
	{
		float[] centroid = {0.f,0.f};
		float 	tmp, 
				area = 0.f;
		int 	next,
				npoints = poly.npoints;
		int[] 	xpoints = poly.xpoints, 
				ypoints = poly.ypoints;
		
		for(int i=0; i<npoints; ++i)
		{
			next = (i+1)%npoints;
			tmp = (xpoints[i]*ypoints[next]-xpoints[next]*ypoints[i]);
			centroid[0] += (xpoints[i]+xpoints[next])*tmp;
			centroid[1] += (ypoints[i]+ypoints[next])*tmp;
			area += tmp;
		}
		centroid[0] = centroid[0]/(3*area);
		centroid[1] = centroid[1]/(3*area);
		return centroid;
	}
	
	public static float computeArea(Polygon poly)
	{
		float	area = 0.f;
		int 	next,
				npoints = poly.npoints;
		int[] 	xpoints = poly.xpoints, 
				ypoints = poly.ypoints;
		
		for(int i=0; i<npoints; ++i)
		{
			next = (i+1)%npoints;
			area += (xpoints[i]*ypoints[next]-xpoints[next]*ypoints[i]);
		}
		return area/2;
	}
	
	public static void reset()
	{
		current_id=0;
	}
	
	public void addLink(Link link) {
		links.add(link);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the area
	 */
	public float getArea() {
		return area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(float area) {
		this.area = area;
	}

	/**
	 * @return the boundary
	 */
	public Rectangle getBoundary() {
		return boundary;
	}

	/**
	 * @param boundary the boundary to set
	 */
	public void setBoundary(Rectangle boundary) {
		this.boundary = boundary;
	}
	
	public long getTrack_id() {
		return track_id;
	}

	public void setTrack_id(long track_id) {
		this.track_id = track_id;
	}
	
	public static long getCurrent_id() {
		return current_id;
	}

	public static void setCurrent_id(long current_id) {
		Cell.current_id = current_id;
	}

	public Polygon getOutline() {
		return outline;
	}
	public void setOutline(Polygon outline) {
		this.outline = outline;
		this.centroid = computeCentroid(outline);
		this.area = computeArea(outline);
	}
	public float[] getCentroid() {
		return centroid;
	}
	public void setCentroid(float[] centroid) {
		this.centroid = centroid;
	}
	
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public LinkedList<Link> getLinks() {
		return links;
	}

	public void setLinks(LinkedList<Link> links) {
		this.links = links;
	}

	public String toString()
	{	
		String 	polygon = "",
				cell = 	"";
		for(int i=0; i<outline.npoints; ++i)
		{
			polygon += "(" + String.valueOf(outline.xpoints[i]) + "," + String.valueOf(outline.ypoints[i]) + ") ";
		}
		cell = "Cell: \n" + 
				"\t ID: " + String.valueOf(id) + "\n" +
				"\t Time: " + String.valueOf(time) + "\n" +
				"\t Area " + String.valueOf(area) + "\n" +
				"\t TrackID: " + String.valueOf(track_id) + "\n" +
				"\t Outline: " + polygon + "\n" +
				"\t Centroid: (" + String.valueOf(centroid[0]) + "," + String.valueOf(centroid[1]) + ")\n" +
				"\t Links:\n";
		for(int i=0; i<links.size(); ++i)
		{
			cell += "\t\t" + links.get(i).toString();
		}
		return cell;
	}
}
