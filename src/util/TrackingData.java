package util;

import java.text.DecimalFormat;
import java.util.LinkedList;

public class TrackingData {
	
	private LinkedList<LinkedList<Cell>> cells;
	private LinkedList<LinkedList<Long>> tracks;

	public TrackingData(LinkedList<LinkedList<Cell>> cells, LinkedList<LinkedList<Long>> tracks)
	{
		this.cells = cells;
		this.tracks = tracks;
	}

	public String composeFileName(String prefix, String postfix, long id)
	{
		String num = "";
		for(int i=0; i<=String.valueOf(cells.size()).length(); i++)
			num += "0";
		DecimalFormat format = new DecimalFormat(num);
		num = format.format(id);
		return prefix  + num + postfix;
	}
	
	public LinkedList<LinkedList<Cell>> getCells() {
		return cells;
	}

	public void setCells(LinkedList<LinkedList<Cell>> cells) {
		this.cells = cells;
	}

	public LinkedList<LinkedList<Long>> getTracks() {
		return tracks;
	}

	public void setTracks(LinkedList<LinkedList<Long>> tracks) {
		this.tracks = tracks;
	}
}
