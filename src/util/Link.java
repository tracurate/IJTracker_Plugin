package util;

public class Link {
	
	public Link(int type, Cell cells[], float probabilities[])
	{
		switch(type)
		{
			case SUCCESSOR:
				this.type = "$SUCCESSOR";
				break;
			case DIVISION:
				this.type = "$DIVISION";
				break;
			case CONTACT:
				this.type = "$CONTACT";
				break;
			default:
				this.type = "$UNKNOWN";
				break;
		}
		this.cells = cells;
		this.probabilities = probabilities;
	}
	public Link(int type, Cell cells[])
	{
		switch(type)
		{
			case SUCCESSOR:
				this.type = "$SUCCESSOR";
				break;
			case DIVISION:
				this.type = "$DIVISION";
				break;
			case CONTACT:
				this.type = "$CONTACT";
				break;
			default:
				this.type = "$UNKNOWN";
				break;
		}
		this.cells = cells;
		this.probabilities = new float[cells.length];
		for(int i=0; i<probabilities.length; ++i)
			probabilities[i] = 1/probabilities.length;
	}
	public Link(int type, Cell cell)
	{
		switch(type)
		{
			case SUCCESSOR:
				this.type = "$SUCCESSOR";
				break;
			case DIVISION:
				this.type = "$DIVISION";
				break;
			case CONTACT:
				this.type = "$CONTACT";
				break;
			default:
				this.type = "$UNKNOWN";
				break;
		}
		this.cells = new Cell[1];
		cells[0] = cell;
		this.probabilities = new float[1];
		probabilities[0] = 1;
	}
	
	public final static int SUCCESSOR = 0;
	public final static int DIVISION = 1;
	public final static int CONTACT = 2;
	
	private String type;
	private Cell cells[];
	private float probabilities[];
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Cell[] getCells() {
		return cells;
	}
	public void setCells(Cell[] cells) {
		this.cells = cells;
	}
	public float[] getProbabilities() {
		return probabilities;
	}
	public void setProbabilities(float[] probabilities) {
		this.probabilities = probabilities;
	}
	
	public String toString() {
		String link = "";
		link += "Link: \n";
		for(int i=0; i<cells.length; ++i)
		{
			link += "\t Object(objectID=" + String.valueOf(cells[i].getId()) + ", frameID=" + String.valueOf(cells[i].getTime()) + ", probability=" + String.valueOf(probabilities[i]) + ")\n";
		}
		return link;
	}
}
