package gui.listeners;

import gui.TrackingDialog;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OkListener implements ActionListener {

	private TrackingDialog parent;
	
	public OkListener(TrackingDialog dialog)
	{
		this.parent = dialog;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		ImagePlus current = WindowManager.getCurrentImage();
		boolean binaryAnd8bit = true;
		
		parent.setEnabled(false);
		try{
			Float.parseFloat(parent.getTextInput().getText());
		}
		catch(NumberFormatException exception)
		{
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.EAST;
			c.weightx = 0.0;
			c.gridwidth = 1;
			c.gridx = 3; c.gridy = 0;
			
			parent.getTextInput().setBackground(new Color(1.f, 0.f, 0.f));
			parent.setEnabled(true);
			return;
		}
		parent.getTextInput().setBackground(new Color(1.f, 1.f, 1.f));
		
		if(current != null)
		{
			ImageStack stack = current.getStack();
			if(stack.getSize() <= 1)
			{
				IJ.error("There have to be at least two images!");
				parent.setEnabled(true);
				return;
			}
			for(int i=1; i<=stack.getSize();++i)
			{
				if(!stack.getProcessor(i).isBinary())
					binaryAnd8bit = false;
			}
			if(!binaryAnd8bit)
			{
				IJ.error("Images have to be binary 8bit images.");
				parent.setEnabled(true);
				return;
			}
			parent.setStack(current);
		}
		else
		{
			IJ.error("There are no images to process. Load an image sequence!");
			parent.setEnabled(true);
			return;
		}
		
		if(parent.getTarget().compareTo("") != 0)
		{
			parent.process();
		}
		else
			IJ.error("Please set the location where to save the tracking data!");
			parent.setEnabled(true);
	}

}
