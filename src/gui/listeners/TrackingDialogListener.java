package gui.listeners;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TrackingDialogListener extends WindowAdapter {
	public void windowClosing(WindowEvent e)
    {
		e.getWindow().dispose();                   // Fenster "killen"
    }           
}
