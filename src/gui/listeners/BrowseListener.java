package gui.listeners;

import gui.TrackingDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class BrowseListener implements ActionListener {
	
	private TrackingDialog parent;
	private String context;

	public BrowseListener(TrackingDialog parent, String context)
	{
		this.parent = parent;
		this.context = context;
	}
	
	public void actionPerformed(ActionEvent e) {
		FileSystemView view = FileSystemView.getFileSystemView();
		JFileChooser  chooser = new JFileChooser(view.getHomeDirectory());
		FileFilter filter = null;
		int status;
		if(context.compareTo("zip-archive")==0)
		{
			filter = new FileNameExtensionFilter("ZIP-archive", "zip");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setFileFilter(filter);
			chooser.setSelectedFile(new File("./project.zip"));
		}
		if(context.compareTo("directory")==0)
		{
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		
		status = chooser.showDialog(parent, "Save to");
		if(status == JFileChooser.APPROVE_OPTION)
		{
			parent.setTarget(chooser.getSelectedFile().getAbsolutePath());
			parent.setTargetPanel();
		}
	}

	/**
	 * @return the context
	 */
	public String getContext() {
		return context;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}

}
