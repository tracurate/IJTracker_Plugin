package gui.listeners;

import gui.TrackingDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChancelListener implements ActionListener {

	private TrackingDialog parent;
	
	public ChancelListener(TrackingDialog dialog)
	{
		this.parent = dialog;
	}
	
	public void actionPerformed(ActionEvent e) {
		parent.dispose();
	}

}
