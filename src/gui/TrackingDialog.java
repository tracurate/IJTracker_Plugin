package gui;

import gui.listeners.BrowseListener;
import gui.listeners.ChancelListener;
import gui.listeners.OkListener;
import gui.listeners.TrackingDialogListener;
import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import image_processing.Juicer;
import io.Connector;
import io.FileConnector;
import io.InvalidTargetException;
import io.XMLExporter;
import io.ZipConnector;

import java.awt.Button;
import java.awt.Choice;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;

import tracking.DistanceTracking;
import util.Cell;
import util.TrackingData;

public class TrackingDialog extends Dialog implements ItemListener {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 2077850609978767924L;
	private final String[] exportChoices = {"zip-archive", "directory"};
	private final String[] versionChoices = {"Celltracker", "tba"};
	private Choice choice;
	private Choice versionChoice;
	private String target = "";
	private Button browseButton = null;
	private Label 	targetLabel = null, 
					descLabel = null, 
					saveLabel = null,
					revision;
	private BrowseListener browseListener;
	private TextField textInput;
	private TextArea description = new TextArea(15, 30);
	private ImagePlus images = null;
	private int version = 0;
	
	public TrackingDialog(String title) {
		super(WindowManager.getCurrentImage()!=null?(Frame)WindowManager.getCurrentImage().getWindow():IJ.getInstance()!=null?IJ.getInstance():new Frame(),
				title);
		this.addWindowListener(new TrackingDialogListener());
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();	
	
		// grid constraints init
		c.anchor = GridBagConstraints.EAST;
		c.weightx = 0.0;
		c.gridwidth = 1;
		c.gridx = 0; c.gridy = 0;
		
		//first line
		this.add(new Label("Minimal cell size:"),c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		this.textInput = new TextField("10", 5);
		this.add(textInput, c);
		
		//second line
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0; c.gridy = 1;
		this.add(new Label("Export to:"),c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		this.choice = new Choice();
		for(int i=0; i<exportChoices.length; ++i)
			choice.add(exportChoices[i]);
		choice.addItemListener(this);
		this.add(choice, c);
		
		//third line
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0; c.gridy = 2;
		this.add(new Label("Version:"),c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		this.versionChoice = new Choice();
		for(int i=0; i<versionChoices.length; ++i)
			versionChoice.add(versionChoices[i]);
		versionChoice.addItemListener(this);
		this.add(versionChoice, c);
		versionChoice.select(0);
		
		//forth and fifth line
		setTargetPanel();
		
		//seventh line
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0; c.gridy = 6;
		Button 	ok =  new Button("Ok"), 
				chancel = new Button("Chancel");
		ok.addActionListener(new OkListener(this));
		chancel.addActionListener(new ChancelListener(this));
		this.add(ok,c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1; c.gridy = 6;
		this.add(chancel,c);
		
		c.anchor = GridBagConstraints.SOUTHWEST;
		c.gridx = 0; c.gridy = 7;
		revision = new Label(util.Revision.REVISION);
		revision.setSize(10, 5);
		revision.setFont(new Font(Font.SANS_SERIF,Font.BOLD, 10));
		this.add(revision,c);
		this.pack();
	}
	
	public void process()
	{
		LinkedList<LinkedList<Cell>> frames = Juicer.extractCells(images.getStack(), Float.parseFloat(textInput.getText()));
		TrackingData data = DistanceTracking.tracking(frames);
		XMLExporter xml;
		Connector connector;
		try {
			if(choice.getSelectedItem().compareTo("zip-archive") == 0)
				connector = new ZipConnector(target);
			else
				connector = new FileConnector(target);
			xml = new XMLExporter(connector, version);
			xml.setDescription(description.getText());
			xml.export(data);
		} 
		catch(InvalidTargetException e) {
			e.printStackTrace();
		}
		Cell.reset();
		this.dispose();
	}
	
	public void setTargetPanel()
	{
		GridBagConstraints c = new GridBagConstraints();
		
		
		c.anchor = GridBagConstraints.EAST;
		c.weightx = 0.0;
		c.gridwidth = 1;
		c.gridx = 0; c.gridy = 3;
		
		if(browseButton!=null)
			this.remove(browseButton);
		if(targetLabel != null)
			this.remove(targetLabel);
		this.validate();
		
		if(target.compareTo("")==0)
		{
			if(saveLabel == null)
			{
				saveLabel = new Label("Save to:");
				this.add(saveLabel,c);
			}
			this.browseButton = new Button("Browse...");
			this.browseListener = new BrowseListener(this, choice.getSelectedItem());
			browseButton.addActionListener(browseListener);
			c.anchor = GridBagConstraints.WEST;
			c.weightx = 0.0;
			c.gridwidth = 1;
			c.gridx = 1; c.gridy = 3;
			this.add(browseButton,c);
		}
		else
		{
			this.browseButton = new Button("Change...");
			this.browseListener = new BrowseListener(this, choice.getSelectedItem());
			browseButton.addActionListener(browseListener);
			this.targetLabel = new Label(target);
			c.anchor = GridBagConstraints.WEST;
			c.weightx = 0.0;
			c.gridwidth = 1;
			c.gridx = 1; c.gridy = 3;
			this.add(targetLabel,c);
			
			c.anchor = GridBagConstraints.WEST;
			c.weightx = 0.0;
			c.gridwidth = 1;
			c.gridx = 1; c.gridy = 4;
			this.add(browseButton,c);
		}
		if(version == 0)
		{
			if(description != null)
				this.remove(description);
			if(descLabel != null)
				this.remove(descLabel);
			this.validate();
		}
		else if(version == 1)
		{
			if(descLabel==null && description == null)
			{
				c.anchor = GridBagConstraints.NORTHEAST;
				c.gridx = 0; c.gridy = 5;
				descLabel = new Label("Description:");
				this.add(descLabel,c);
				c.anchor = GridBagConstraints.WEST;
				c.gridx = 1;
				this.add(description,c);
				this.validate();
			}
		}
		this.pack();
		this.repaint();
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the textInput
	 */
	public TextField getTextInput() {
		return textInput;
	}

	/**
	 * @param textInput the textInput to set
	 */
	public void setTextInput(TextField textInput) {
		this.textInput = textInput;
	}

	/**
	 * @return the stack
	 */
	public ImagePlus getStack() {
		return images;
	}

	/**
	 * @param stack the stack to set
	 */
	public void setStack(ImagePlus images) {
		this.images = images;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getSource().equals(choice))
		{
			if(target.compareTo("")==0)
			{
				browseListener.setContext(choice.getSelectedItem());	
			}
			else
			{
				target = "";
				this.remove(browseButton);
				setTargetPanel();	
			}
		}
		else if(e.getSource().equals(versionChoice))
		{
			version  = versionChoice.getSelectedIndex();
			setTargetPanel();
		}
	}
}
