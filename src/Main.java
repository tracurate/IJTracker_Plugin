import gui.TrackingDialog;
import ij.ImagePlus;
import ij.ImageStack;
import ij.Prefs;
import ij.plugin.FolderOpener;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Prefs.blackBackground=true;
		//ImagePlus current = IJ.openImage("/Users/kthierbach/Documents/data/artificial/binary/bin0001.png");
		//ImagePlus current = IJ.openImage("/Users/kthierbach/Dropbox/zebrafish/data/fast_sox17/binarized/bin0001.png");
		ImagePlus current = FolderOpener.open("/Users/kthierbach/Documents/data/artificial/binary");
		//ImagePlus current = FolderOpener.open("/Users/kthierbach/Dropbox/zebrafish/data/fast_sox17/binarized");
		//ImagePlus current = FolderOpener.open("/Users/kthierbach/test2/img");
		
		System.out.println("Is virtual " + String.valueOf(current.getStack().isVirtual()));
		System.out.println("Bit depth is " + String.valueOf(current.getBitDepth()));
		System.out.println("Size of stack is " + String.valueOf(current.getImageStackSize()));
		
		ImageStack stack  = current.getStack();
		
		ImageStack binaryStack = new ImageStack(stack.getWidth(),stack.getHeight());
		for(int i=1; i<=stack.getSize(); ++i)
		{
			binaryStack.addSlice(stack.getProcessor(i).convertToByte(true));
		}
		
		TrackingDialog dialog = new TrackingDialog("Tracking preferences");
		dialog.setVisible(true);
		
		
		/*LinkedList<LinkedList<Cell>> frames = Juicer.extractCells(binaryStack, 10.f);
		
		TrackingData data = DistanceTracking.tracking(frames);
		XMLExporter xml;
		try {
			xml = new XMLExporter(new ZipConnector("/Users/kthierbach/test.zip"));
			xml.export(data);
		} catch (InvalidTargetException e) {
			e.printStackTrace();
		}*/
		
		/*ListIterator<Cell> frameIt;
		ListIterator<LinkedList<Cell>> frameListIt = frames.listIterator();
		while(frameListIt.hasNext())
		{
			
			frameIt = frameListIt.next().listIterator();
			while(frameIt.hasNext())
				System.out.print(frameIt.next().toString());
		}*/
	}

}
