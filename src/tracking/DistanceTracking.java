package tracking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import util.Cell;
import util.Link;
import util.TrackingData;

public class DistanceTracking {
	public static TrackingData tracking(LinkedList<LinkedList<Cell>> frames)
	{
		LinkedList<Cell> 	frame1, frame2;
		ArrayList<Integer>	distancesFrom, distancesTo;
		HashMap<Long,Long> mappings = new HashMap<Long, Long>((int) Cell.getCurrent_id()-1);
		ListIterator<Integer> distIt;
		ListIterator<Cell> cellIt;
		int index,
			nFrames = frames.size()-1;//size of frames minus 1
		LinkedList<LinkedList<Cell>> cells;
		LinkedList<LinkedList<Long>> tracks;
		
		for(int i=0; i<nFrames;++i)
		{
			frame1 = frames.get(i);
			frame2 = frames.get(i+1);
			
			distancesFrom = computeMinimalDistances(frame1,frame2);//list of length equal to frame1, each entry points to an element in frame2
			distancesTo = computeMinimalDistances(frame2,frame1);//list of length equal to frame2, each entry points to an element in frame1
			if(distancesFrom != null) //if distancesFrom is null then distancesTo is too, thus testing one of the lists is sufficient
			{
				distIt = distancesFrom.listIterator();
				while(distIt.hasNext())
				{
					index = distIt.next();
					if(distIt.previousIndex() == distancesTo.get(index))
						mappings.put(frame1.get(distIt.previousIndex()).getId(), frame2.get(index).getId());
					else
						mappings.put(frame1.get(distIt.previousIndex()).getId(), null);
				}
			}
			else
			{
				cellIt=frame1.listIterator();
				while(cellIt.hasNext())
				{	
					mappings.put(cellIt.next().getId(), null);
				}
			}
		}
		//add mappings id->null for last frame to end tracks properly
		frame1 = frames.getLast();
		cellIt = frame1.listIterator();
		while(cellIt.hasNext())
		{
			mappings.put(cellIt.next().getId(), null);
		}
		
		tracks=computeTracks(mappings);
		cells=updateTrackIDs(frames,tracks);
		return new TrackingData(cells,tracks);
	}
	
	/*
	 * This function takes two lists of cells and returns a list of cell id's. The length of the returned list is the same as
	 * the length of list frame1 and the cell id's from the cells in the list frame2. Each entry of the returned list
	 * gives the mapping from the cells in frame1 to the cell id's of the cells in frame2.
	 */
	private static ArrayList<Integer> computeMinimalDistances(LinkedList<Cell> frame1, LinkedList<Cell> frame2)
	{
		ArrayList<Integer> 	distances = new ArrayList<Integer>(frame1.size());
		double[]			tmpDistances = new double[frame2.size()];
		float[]				centroid1,
							centroid2;
		ListIterator<Cell> 	cellIt1 = frame1.listIterator(),
							cellIt2;
		
		//returns null if one of the given lists has no elements (no cells were found)
		if(frame1.size() == 0 || frame2.size() == 0)
			return null;
		
		while(cellIt1.hasNext())
		{
			centroid1=cellIt1.next().getCentroid();
			cellIt2 = frame2.listIterator();
			while(cellIt2.hasNext())
			{
				centroid2=cellIt2.next().getCentroid();
				tmpDistances[cellIt2.previousIndex()] =  Math.sqrt(Math.pow(centroid1[0]-centroid2[0],2) + Math.pow(centroid1[1]-centroid2[1],2));
			}
			distances.add(cellIt1.previousIndex(), minimalValueIndex(tmpDistances));
		}
		return distances;
	}
	
	private static LinkedList<LinkedList<Long>> computeTracks(HashMap<Long, Long> mappings)
	{
		LinkedList<LinkedList<Long>> tracks  = new LinkedList<LinkedList<Long>>();
		LinkedList<Long> track;
		LinkedList<Long> keys = new LinkedList<Long>();
		ListIterator<Long> idIt;
		Long key,tmpKey;
		long nCells = Cell.getCurrent_id();
		
		for(long i=0; i<nCells; ++i)
			keys.add(new Long(i));
		idIt=keys.listIterator();
		while(idIt.hasNext())
		{
			key=idIt.next();
			track  = new LinkedList<Long>();
			while(mappings.containsKey(key))
			{
				track.add(key);
				tmpKey=mappings.get(key);
				mappings.remove(key);
				key=tmpKey;
			}
			if(track.size() != 0)
				tracks.add(track);
		}
		return tracks;
	}
	
	private static int minimalValueIndex(double[] list)
	{
		double min = list[0];
		int index = 0;

		if(list.length == 1)
			return index;//shortest distance is to element 0, because there is only one
		for(int i=1; i<list.length; ++i)
		{
			if(list[i]<min)
			{
				min = list[i];
				index = i;
			}
		}
		return index;
	}
	
	private static LinkedList<LinkedList<Cell>> updateTrackIDs(LinkedList<LinkedList<Cell>> cells,LinkedList<LinkedList<Long>> tracks)
	{
		HashMap<Long,Long> 				cell2TrackMapping = new HashMap<Long, Long>((int) Cell.getCurrent_id()-1);
		HashMap<Long,Cell> 				cellID2Cell = new HashMap<Long, Cell>((int) Cell.getCurrent_id()-1);
		ListIterator<LinkedList<Long>> 	tracksIt = tracks.listIterator();
		ListIterator<Long>				singleTrackIt;
		ListIterator<LinkedList<Cell>>	cellListIt = cells.listIterator();
		ListIterator<Cell>				cellIt;
		LinkedList<Long> 				track; 
		Long							trackID;
		Cell							cell, nextCell;
		int								cellID;
		
		while(tracksIt.hasNext())
		{
			singleTrackIt=tracksIt.next().listIterator();
			trackID = new Long((long)tracksIt.previousIndex());
			while(singleTrackIt.hasNext())
			{
				cell2TrackMapping.put(singleTrackIt.next(),trackID);
			}
		}
		while(cellListIt.hasNext())
		{
			cellIt = cellListIt.next().listIterator();
			while(cellIt.hasNext())
			{
				cell = cellIt.next();
				cellID2Cell.put(cell.getId(),cell);
			}
		}
		
		cellListIt = cells.listIterator();
		while(cellListIt.hasNext())
		{
			cellIt = cellListIt.next().listIterator();
			while(cellIt.hasNext())
			{
				cell = cellIt.next();
				trackID = cell2TrackMapping.get(new Long(cell.getId()));
				cell.setTrack_id(trackID);
				track = tracks.get(trackID.intValue());
				cellID = track.indexOf(new Long(trackID))+1;
				if(cellID < track.size())
				{
					nextCell = cellID2Cell.get(track.get(cellID));
					cell.addLink(new Link(Link.SUCCESSOR, nextCell));
				}
				cellIt.set(cell);
			}
		}
		return cells;
	}
}
