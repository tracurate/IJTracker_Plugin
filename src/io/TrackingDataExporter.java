package io;

import util.TrackingData;

abstract class TrackingDataExporter {
	
	protected Connector connector = null;
	
	public TrackingDataExporter(Connector connector)
	{
		this.connector = connector;
	}
	abstract public void export(TrackingData data);
	
	/**
	 * @return the connector
	 */
	public Connector getConnector() {
		return connector;
	}
	/**
	 * @param connector the connector to set
	 */
	public void setConnector(Connector connector) {
		this.connector = connector;
	}
}
