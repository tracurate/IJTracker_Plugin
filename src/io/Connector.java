package io;

import java.io.OutputStream;

public abstract class Connector {
	
	protected String target;
	
	public Connector(String target) throws InvalidTargetException
	{
		this.target = target;
	}
	
	abstract public OutputStream addEntry(String name) throws InvalidTargetException;
	abstract public void close();
	abstract public void setTarget(String target) throws InvalidTargetException;
}
