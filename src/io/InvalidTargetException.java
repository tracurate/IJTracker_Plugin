package io;

public class InvalidTargetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2827883986769576509L;

	public InvalidTargetException() {
		super();
	}
	
	public InvalidTargetException(String message) {
		super(message);
	}

	public InvalidTargetException(String message, Throwable e) 
	{
		super(message, e);
	}
}
