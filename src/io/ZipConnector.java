package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipConnector extends Connector {
	
	private ZipOutputStream out;

	public ZipConnector(String target) throws InvalidTargetException 
	{
		super(target);
		try {
			this.out = new ZipOutputStream(new FileOutputStream(target));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public OutputStream addEntry(String name) throws InvalidTargetException {
		try {
			out.putNextEntry(new ZipEntry(name));
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return out;	
	}

	@Override
	public void setTarget(String target) throws InvalidTargetException {
		this.target = target;
	}
	
	@Override
	public void close() {
		if(out != null)
		{
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
