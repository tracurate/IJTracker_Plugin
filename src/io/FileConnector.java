package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileConnector extends Connector {

	private FileOutputStream out = null;
	
	public FileConnector(String target) throws InvalidTargetException {
		super(target);
		File file = new File(target);
		if(!file.exists())
		{
			file.mkdirs();
		}
		else if(!file.isDirectory() && file.exists())
		{
			throw new InvalidTargetException();
		}
	}

	@Override
	public OutputStream addEntry(String name) throws InvalidTargetException {
		File file;
		if(out != null)
		{
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		file = new File(target,name);
		try {
			if(!new File(file.getParent()).exists())
			{
				new File(file.getParent()).mkdir();
			}
			out = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return out;
	}

	@Override
	public void close() {
		if(out != null)
		{
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setTarget(String target) throws InvalidTargetException {
		this.target = target;
	}
}
