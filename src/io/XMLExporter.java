package io;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import util.Cell;
import util.Link;
import util.TrackingData;

public class XMLExporter extends TrackingDataExporter {
	
	public XMLExporter(Connector connector, int version) {
		super(connector);
		this.version = version;
	}

	private int version = 0;
	private String description = "";

	@Override
	public void export(TrackingData data) {
		switch(version)
		{
			case 0:
				exportTrackFile(data);
				exportMetaFiles(data);
				break;
			case 1:
				exportData(data);
				break;
		}
		connector.close();
	}
	
	private void exportMetaFiles(TrackingData data) {
		ListIterator<LinkedList<Cell>> cellListIt = data.getCells().listIterator();
		ListIterator<Cell> cellIt;
		float[] point = new float[2];
		int[] intPoint = new int[2];
		Rectangle rect;
		Polygon poly;
		
		Element rootElement, object, objectID, objectCenter, objectBoundingBox, objectOutline, trackID;
		
		DocumentBuilderFactory docFactory;
		DocumentBuilder docBuilder;
		TransformerFactory transformerFactory;
		Transformer transformer;
		Document doc;
		
		try {
			docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "1");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return;
		}
		
		while(cellListIt.hasNext())
		{
			doc = docBuilder.newDocument();
			Cell cell;
			cellIt = cellListIt.next().listIterator();
			rootElement = doc.createElement("Frame_" + String.valueOf(cellListIt.previousIndex()+1));
			while(cellIt.hasNext())
			{
				cell = cellIt.next();
				//cell object
				object = doc.createElement("Object");
				//objectID
				objectID = doc.createElement("ObjectID");
				objectID.appendChild(createValueElement(doc, cell.getId()+1));
				//objectCenter
				objectCenter = doc.createElement("ObjectCenter");
				objectCenter.appendChild(createPointElement(doc, cell.getCentroid()));
				//objectBoundingBox
				objectBoundingBox =  doc.createElement("ObjectBoundingBox");
				rect = cell.getBoundary();
				intPoint[0] = rect.x;
				intPoint[1] = rect.y;
				objectBoundingBox.appendChild(createPointElement(doc,intPoint));
				intPoint[0] += rect.width;
				intPoint[1] += rect.height;
				objectBoundingBox.appendChild(createPointElement(doc,intPoint));
				//object outline
				objectOutline = doc.createElement("Outline");
				poly = cell.getOutline();
				for(int i=0; i<poly.npoints; ++i)
				{
					point[0] = poly.xpoints[i];
					point[1] = poly.ypoints[i];
					objectOutline.appendChild(createPointElement(doc, point));
				}
				//object track id
				trackID = doc.createElement("TrackID");
				trackID.appendChild(createValueElement(doc, cell.getTrack_id()+1));
				
				//add object elements
				object.appendChild(objectID);
				object.appendChild(objectCenter);
				object.appendChild(objectBoundingBox);
				object.appendChild(objectOutline);
				object.appendChild(trackID);
				
				//add object to frame
				rootElement.appendChild(object);
			}
			doc.appendChild(rootElement);
			try {
				StreamResult result = new StreamResult(connector.addEntry(data.composeFileName("xml/meta", ".xml", cellListIt.previousIndex())));
				transformer.transform(new DOMSource(doc), result);
			} catch (TransformerException e) {
				e.printStackTrace();
			}
			catch (InvalidTargetException e) {
				e.printStackTrace();
			}
		}
	}

	private void exportTrackFile(TrackingData data)
	{
		ListIterator<LinkedList<Long>> trackListIt = data.getTracks().listIterator();
		ListIterator<LinkedList<Cell>> cellListIt = data.getCells().listIterator();
		ListIterator<Long> trackIt;
		ListIterator<Cell> cellIt;
		HashMap<Long,Long> cellID2frameID = new HashMap<Long, Long>((int) Cell.getCurrent_id()-1);
		Long trackElement;
		
		Element rootElement;
		
		//track file elements
		Element track, trackID, object, objectID, time;
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return;
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return;
		}
		Document doc;
		DOMSource source;
		
		while(cellListIt.hasNext())
		{
			cellIt=cellListIt.next().listIterator();
			while(cellIt.hasNext())
			{
				cellID2frameID.put(cellIt.next().getId(),(long) cellListIt.previousIndex());
			}
		}
		//reset list iterator
		cellListIt = data.getCells().listIterator();
		// create track xml file
		doc = docBuilder.newDocument();
		rootElement = doc.createElement("Tracks");
		doc.appendChild(rootElement);
		while(trackListIt.hasNext())
		{
			// track elements
			track = doc.createElement("Track");
			rootElement.appendChild(track);
			trackIt = trackListIt.next().listIterator();
			trackID = doc.createElement("TrackID");
			trackID.appendChild(doc.createTextNode(String.valueOf(trackListIt.previousIndex()+1)));
			track.appendChild(trackID);
			while(trackIt.hasNext())
			{
				trackElement = trackIt.next();
				object = doc.createElement("object");
				objectID = doc.createElement("ObjectID");
				objectID.appendChild(doc.createTextNode(String.valueOf(trackElement+1)));
				time = doc.createElement("Time");
				time.appendChild(doc.createTextNode(String.valueOf(cellID2frameID.get(trackElement)+1)));
				object.appendChild(objectID);
				object.appendChild(time);
				track.appendChild(object);
			}
			
		}
		
		try {
			source = new DOMSource(doc);
			StreamResult result = new StreamResult(connector.addEntry("tracksXML.xml"));
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		catch (InvalidTargetException e) {
			e.printStackTrace();
		}
	}
	
	private void exportData(TrackingData data) {
		ListIterator<LinkedList<Cell>> cellListIt;
		ListIterator<Cell> cellIt;
		Cell cell;
		
		DateFormat dateFormat;
		
		SAXTransformerFactory transformerFactory;
		TransformerHandler handler;
		Transformer transformer;
		StreamResult result;
		AttributesImpl atts;
		String value;
		
		cellListIt = data.getCells().listIterator();
		try {
			transformerFactory = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
			handler = transformerFactory.newTransformerHandler();
			transformer = handler.getTransformer();
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "1");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			result = new StreamResult(connector.addEntry("data-frame.xml"));
			handler.setResult(result);
			handler.startDocument();
			atts = new AttributesImpl();
	
			//create root
			atts.addAttribute("", "", "version", "NMTOKEN", "1.0");
			handler.startElement("", "", "data_frame", atts);
			
			// set description
			atts.clear();
			handler.startElement("", "", "description", atts);
			handler.startElement("", "", "date", atts);
			dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			value = dateFormat.format(new Date());
			handler.characters(value.toCharArray(), 0, value.length());
			handler.endElement("", "", "date");
			handler.startElement("", "", "number_of_frames", atts);
			value = NumberFormat.getInstance().format((long)data.getCells().size());
			handler.characters(value.toCharArray(), 0, value.length());
			handler.endElement("", "", "number_of_frames");
			handler.startElement("", "", "number_of_tracks", atts);
			value = NumberFormat.getInstance().format((long)data.getTracks().size());
			handler.characters(value.toCharArray(), 0, value.length());
			handler.endElement("", "", "number_of_tracks");
			handler.startElement("", "", "total_number_of_objects", atts);
			value = NumberFormat.getInstance().format((long)Cell.getCurrent_id());
			handler.characters(value.toCharArray(), 0, value.length());
			handler.endElement("", "", "total_number_of_objects");
			handler.startElement("", "", "details", atts);
			handler.characters(description.toCharArray(), 0, description.length());
			handler.endElement("", "", "details");
			handler.endElement("", "", "description");
			//start with frames
			handler.startElement("", "", "frames", atts);
	
			cellListIt = data.getCells().listIterator();
			while(cellListIt.hasNext())
			{
				cellIt = cellListIt.next().listIterator();
				atts.clear();
				atts.addAttribute("", "", "id", "NMTOKEN", String.valueOf(cellListIt.previousIndex()));
				atts.addAttribute("", "", "number_of_objects", "NMTOKEN", String.valueOf(data.getCells().get(cellListIt.previousIndex()).size()));
				handler.startElement("", "", "frame", atts);
				atts.clear();
				
				while(cellIt.hasNext())
				{
					cell = cellIt.next();
					handler.startElement("", "", "object", atts);
					handler.startElement("", "", "id", atts);
					value = String.valueOf(cell.getId());
					handler.characters(value.toCharArray(), 0, value.length());
					handler.endElement("", "", "id");
					handler.startElement("", "", "centroid", atts);
					createShortPointElement(handler, cell.getCentroid());
					handler.endElement("", "", "centroid");
					handler.startElement("", "", "outline", atts);
					createOutlineElement(handler, cell.getOutline());
					handler.endElement("", "", "outline");
					handler.startElement("", "", "links", atts);
					createLinksElement(handler,cell);
					handler.endElement("", "", "links");
					handler.endElement("", "", "object");
				}
				handler.endElement("", "", "frame");
			}
			handler.endElement("", "", "frames");
			handler.endElement("", "", "data_frame");
			handler.endDocument();
		}
		catch(SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Element createPointElement(Document doc, float[] array) {
		Element point 	= doc.createElement("point"),
				x		= doc.createElement("x"),
				y		= doc.createElement("y");
		x.appendChild(doc.createTextNode(String.valueOf(array[0])));
		y.appendChild(doc.createTextNode(String.valueOf(array[1])));
		point.appendChild(x);
		point.appendChild(y);
		return point;
	}
	private Element createPointElement(Document doc, int[] array) {
		Element point 	= doc.createElement("point"),
				x		= doc.createElement("x"),
				y		= doc.createElement("y");
		x.appendChild(doc.createTextNode(String.valueOf(array[0])));
		y.appendChild(doc.createTextNode(String.valueOf(array[1])));
		point.appendChild(x);
		point.appendChild(y);
		return point;
	}
	private void createShortPointElement(TransformerHandler handler, float[] array) {
		AttributesImpl atts = new AttributesImpl();
		atts.addAttribute("", "", "x", "NMTOKEN", String.valueOf(array[0]));
		atts.addAttribute("", "", "y", "NMTOKEN", String.valueOf(array[0]));
		try {
			handler.startElement("", "", "p", atts);
			handler.endElement("", "", "p");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void createOutlineElement(TransformerHandler handler, Polygon poly) {
		AttributesImpl atts = new AttributesImpl();
		for(int i=0; i<poly.xpoints.length; ++i)
		{
			atts.clear();
			atts.addAttribute("", "", "x", "NMTOKEN", String.valueOf(poly.xpoints[i]));
			atts.addAttribute("", "", "y", "NMTOKEN", String.valueOf(poly.ypoints[i]));
			try {
				handler.startElement("", "", "p", atts);
				handler.endElement("", "", "p");
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	private void createLinksElement(TransformerHandler handler, Cell cell)
	{
		LinkedList<Link> links = cell.getLinks();
		Link link;
		AttributesImpl atts = new AttributesImpl();
		for(int i=0; i<links.size(); ++i)
		{
			try {
				link = links.get(i);
				atts.addAttribute("", "", "type", "CDATA", link.getType());
				handler.startElement("", "", "link", atts);
				atts.clear();
				for(int j=0; j<link.getCells().length; ++j)
				{
					atts.addAttribute("", "", "objectID", "NMTOKEN", String.valueOf(link.getCells()[j].getId()));
					atts.addAttribute("", "", "frameID", "NMTOKEN", String.valueOf(link.getCells()[j].getTime()));
					atts.addAttribute("", "", "probability", "NMTOKEN", String.valueOf(link.getProbabilities()[j]));
					handler.startElement("", "", "object", atts);
					handler.endElement("", "", "object");
					atts.clear();
				}
				handler.endElement("", "", "link");
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	private Element createValueElement(Document doc, long value)
	{
		Element valueElement = doc.createElement("value");
		valueElement.appendChild(doc.createTextNode(String.valueOf(value)));
		return valueElement;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
}
