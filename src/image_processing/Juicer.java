package image_processing;

import ij.ImageStack;
import ij.Prefs;
import ij.gui.Wand;
import ij.process.BinaryProcessor;
import ij.process.ByteProcessor;
import ij.process.FloodFiller;

import java.awt.Polygon;
import java.util.LinkedList;

import util.Cell;

//Juicer traces components with 4-connected pixels
public class Juicer {
	
	public static LinkedList<LinkedList<Cell>> extractCells(ImageStack stack, float minCellSize)
	{
		LinkedList<LinkedList<Cell>> frames = new LinkedList<LinkedList<Cell>>();
		for(int i=1; i<=stack.getSize(); ++i)
		{
			frames.add(extractCells(new BinaryProcessor(new ByteProcessor(stack.getProcessor(i).convertToByte(true).createImage())), minCellSize, i-1));
		}
		return frames;
	}
	public static LinkedList<Cell> extractCells(BinaryProcessor imageProcessor, float minCellSize, long time)
	{
		LinkedList<Cell> frame = new LinkedList<Cell>();
		int foreground = 255;
		int height = imageProcessor.getHeight(),
			width = imageProcessor.getWidth();
		Wand wand = new Wand(imageProcessor);
		FloodFiller flood = new FloodFiller(imageProcessor);
		Cell cell;

		
		//set filling color to background color
		if(Prefs.blackBackground)
		{
			imageProcessor.setColor(0);
		}
		else
		{
			foreground=0;
			imageProcessor.setColor(255);
		}
		
		for(int x=0; x<width;++x)
		{
			for(int y=0;y<height;++y)
			{
				if(imageProcessor.get(x,y) == foreground)
				{
					wand.autoOutline(x, y);
					flood.fill(x, y);
					if(Cell.computeArea(new Polygon(wand.xpoints, wand.ypoints, wand.npoints))>minCellSize)
					{
						cell = new Cell(new Polygon(wand.xpoints, wand.ypoints, wand.npoints), time);
						frame.add(cell);
					}
				}
			}
		}
		return frame;
	}
}
